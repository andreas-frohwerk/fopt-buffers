package de.frohwerk.fopt.io;

import org.junit.jupiter.api.RepeatedTest;

import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import static com.google.common.truth.Truth.assertThat;

class InMemoryPipeTest {
    @RepeatedTest(2000)
    void name() throws Exception {

        final InMemoryPipe classUnderTest = new InMemoryPipe();
        final InMemoryPipe.PipeOutputStream outputStream = classUnderTest.getOutputStream();
        final InMemoryPipe.PipeInputStream inputStream = classUnderTest.getInputStream();

        final CompletableFuture<String> result = new CompletableFuture<>();

        final Thread sender = new Thread(() -> {
            try {
                for (int i = 0; i < 4; i++) {
                    outputStream.write("0123456789".getBytes());
                }
            } finally {
                outputStream.close();
            }
        }, "sender");
        sender.start();

        final Random random = new Random(System.currentTimeMillis());
        final Thread receiver = new Thread(() -> {
            final StringBuilder sb = new StringBuilder();

            byte[] buf = new byte[random.nextInt(5) + 4];
            int available = inputStream.read(buf);
            while (available > 0) {
                sb.append(new String(buf, 0, available));
                available = inputStream.read(buf);
            }

            result.complete(sb.toString());
        }, "receiver");
        receiver.start();

        assertThat(result.get(5, TimeUnit.SECONDS)).isEqualTo("0123456789012345678901234567890123456789");

        sender.join(200);
        assertThat(sender.isAlive()).isFalse();
        receiver.join(200);
        assertThat(receiver.isAlive()).isFalse();
    }
}