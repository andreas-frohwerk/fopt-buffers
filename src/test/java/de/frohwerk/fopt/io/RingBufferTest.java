package de.frohwerk.fopt.io;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.ArgumentsSource;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

import static com.google.common.truth.Truth.assertThat;

class RingBufferTest {
    private final String[] messages = {"Das ", "ist ein ", "Test"};

    @DisplayName("Transfer with...")
    @ArgumentsSource(RandomBufferSizes.class)
    @ParameterizedTest(name = "...RingBuffer size {0} and result buffer size {1}")
    void basicUseCase(int capacity, int bufferSize) throws Exception {
        final RingBuffer classUnderTest = new RingBuffer(capacity);
        final CompletableFuture<String> resultFuture = new CompletableFuture<>();

        final Thread producer = fork("producer", () -> {
            for (final String message : messages) {
                classUnderTest.write(message.getBytes(StandardCharsets.UTF_8));
            }
            classUnderTest.close();
        });

        final Thread consumer = fork("consumer", () -> {
            final StringBuilder sb = new StringBuilder();
            byte[] buffer = new byte[bufferSize];
            int available = classUnderTest.read(buffer);
            while (available > 0) {
                sb.append(StandardCharsets.UTF_8.decode(ByteBuffer.wrap(buffer, 0, available)));
                available = classUnderTest.read(buffer);
            }
            resultFuture.complete(sb.toString());
        });

        producer.join(200);
        consumer.join(200);

        assertThat(resultFuture.get()).isEqualTo(String.join("", messages));
    }

    private static Thread fork(final String threadName, final Runnable task) {
        final Thread thread = new Thread(task, threadName);
        thread.start();
        return thread;
    }

    private static class RandomBufferSizes implements ArgumentsProvider {
        private final java.util.Random random = new java.util.Random();
        @Override
        public Stream<? extends Arguments> provideArguments(final ExtensionContext context) {
            return Stream.generate(() -> Arguments.of(random.nextInt(6) + 1, random.nextInt(6) + 1)).limit(2000);
        }
    }
}
