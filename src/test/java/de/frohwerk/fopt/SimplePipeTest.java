package de.frohwerk.fopt;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.RepeatedTest;

import java.security.SecureRandom;
import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static com.google.common.truth.Truth.assertThat;

class SimplePipeTest {
    private final ExecutorService executorService = Executors.newFixedThreadPool(2);

    @AfterEach
    void tearDown() throws InterruptedException {
        executorService.shutdownNow();
        executorService.awaitTermination(5, TimeUnit.SECONDS);
    }

    @RepeatedTest(2000)
    void writeAndRead() throws Exception {
        final String testMessage = "0123456789012345678901234567890123456789";
        final SimplePipe classUnderTest = new SimplePipe();

        final CompletableFuture<String> result = new CompletableFuture<>();

        final Thread sender = new Thread(() -> {
            try {
                classUnderTest.send(testMessage.getBytes());
            } finally {
                classUnderTest.close();
            }
        }, "sender");
        sender.start();

        final Random random = new Random(System.currentTimeMillis());
        final Thread receiver = new Thread(() -> {
            final StringBuilder sb = new StringBuilder();

            byte[] buf = new byte[random.nextInt(9) + 1];
            int available = classUnderTest.receive(buf, 0, buf.length);
            while (available > 0) {
                sb.append(new String(buf, 0, available));
                buf = new byte[random.nextInt(9) + 1];
                available = classUnderTest.receive(buf, 0, buf.length);
            }

            result.complete(sb.toString());
        }, "receiver");
        receiver.start();

        assertThat(result.get(5, TimeUnit.SECONDS)).isEqualTo(testMessage);

        sender.join(200);
        assertThat(sender.isAlive()).isFalse();
        receiver.join(200);
        assertThat(receiver.isAlive()).isFalse();
    }
}