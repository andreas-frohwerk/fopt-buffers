package de.frohwerk.fopt.io;

import java.io.InputStream;
import java.io.OutputStream;

public interface Pipe {
    InputStream getInputStream();

    OutputStream getOutputStream();
}
