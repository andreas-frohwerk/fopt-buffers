package de.frohwerk.fopt.io;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class RingBuffer {
    private static final Logger logger = LogManager.getLogger(RingBuffer.class);

    private final byte[] data;
    private final int capacity;

    private boolean open = true;

    private int head = 0;
    private int tail = 0;

    private int count = 0;

    private final Lock lock = new ReentrantLock();

    private final Condition consumer = lock.newCondition();
    private final Condition producer = lock.newCondition();

    public RingBuffer() {
        this(10);
    }

    public RingBuffer(final int bufferSize) {
        data = new byte[bufferSize];
        capacity = data.length;
    }

    public void write(final byte[] source) {
        int offset = 0;
        int length = source.length;
        while (length > 0) {
            lock.lock();
            try {
                while (capacity == count && open) {
                    try {
                        producer.await();
                    } catch (final InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                }
                if (!open) throw new IllegalStateException("Buffer is closed");
                // Calculate first chunk length
                int chunkSize = Math.min(tail < head ? head - tail : capacity - tail, length);
                // Copy first chunk to the buffer
                logger.debug("write '{}' into buffer[head={},tail={},count={}]", new String(source, offset, chunkSize), head, tail, count);
                logger.trace("write: System.arraycopy(byte[{}], {}, byte[{}], {}, {})", source.length, offset, data.length, tail, chunkSize);
                try {
                    System.arraycopy(source, offset, data, tail, chunkSize);
                } catch (final ArrayIndexOutOfBoundsException ex) {
                    logger.error("error: System.arraycopy(byte[{}], {}, byte[{}], {}, {})", source.length, offset, data.length, tail, chunkSize, ex);
                    throw ex;
                }
                // Update source metadata
                offset = offset + chunkSize;
                length = length - chunkSize;
                // Update buffer metadata
                count = count + chunkSize;
                tail = (tail + chunkSize) % capacity;
                // Write second chunk in case there is space available at the start of the buffer
                chunkSize = Math.min(capacity - count, length);
                if (chunkSize > 0) {
                    // Update source metadata
                    logger.debug("write '{}' into buffer[head={},tail={},count={}]", new String(source, offset, chunkSize), head, tail, count);
                    logger.trace("write: System.arraycopy(byte[{}], {}, byte[{}], {}, {})", source.length, offset, data.length, tail, chunkSize);
                    try {
                        System.arraycopy(source, offset, data, tail, chunkSize);
                    } catch (final ArrayIndexOutOfBoundsException ex) {
                        logger.error("error: System.arraycopy(byte[{}], {}, byte[{}], {}, {})", source.length, offset, data.length, tail, chunkSize, ex);
                        throw ex;
                    }
                    // Update source metadata
                    offset = offset + chunkSize;
                    length = length - chunkSize;
                    // Update buffer metadata
                    count = count + chunkSize;
                    tail = (tail + chunkSize) % capacity;
                }
                // Wake up a consumer if space gets scarce
                if (count >= capacity - (capacity / 4)) consumer.signal();
            } finally {
                lock.unlock();
            }
        }
    }

    public int read(final byte[] destination) {
        int offset = 0;
        int length = destination.length;
        lock.lock();
        try {
            while (count == 0 && open) {
                try {
                    consumer.await();
                } catch (final InterruptedException ex) {
                    Thread.currentThread().interrupt();
                }
            }
            if (!open && count == 0) return -1;
            // Calculate first chunk size
            int chunkSize = Math.min(head < tail ? tail - head : capacity - head, length);
            // Read first chunk into the destination buffer
            logger.debug("read '{}' from buffer[head={},tail={},count={}]", new String(data, head, chunkSize), head, tail, count);
            logger.trace("read:  System.arraycopy(byte[{}], {}, byte[{}], {}, {})", data.length, head, destination.length, offset, chunkSize);
            try {
                System.arraycopy(data, head, destination, offset, chunkSize);
            } catch (ArrayIndexOutOfBoundsException ex) {
                logger.error("error: System.arraycopy(byte[{}], {}, byte[{}], {}, {})", data.length, head, destination.length, offset, chunkSize, ex);
            }
            // Update destination metadata
            int bytesAvailable = chunkSize;
            offset = offset + chunkSize;
            length = length - chunkSize;
            // Update buffer metadata
            count = count - chunkSize;
            head = (head + chunkSize) % capacity;
            // Read second chunk in case there is data available at the start of the buffer
            chunkSize = Math.min(count, length);
            if (chunkSize > 0) {
                logger.debug("read '{}' from buffer[head={},tail={},count={}]", new String(data, head, chunkSize), head, tail, count);
                logger.trace("read:  System.arraycopy(byte[{}], {}, byte[{}], {}, {})", data.length, head, destination.length, offset, chunkSize);
                try {
                    System.arraycopy(data, head, destination, offset, chunkSize);
                } catch (ArrayIndexOutOfBoundsException ex) {
                    logger.error("error: System.arraycopy(byte[{}], {}, byte[{}], {}, {})", data.length, head, destination.length, offset, chunkSize, ex);
                }
                // Update destination metadata
                bytesAvailable = bytesAvailable + chunkSize;
                // Update buffer metadata
                count = count - chunkSize;
                head = (head + chunkSize) % capacity;
            }
            // Wake up a producer if data gets scarce
            if (count <= capacity / 4) producer.signal();
            return bytesAvailable;
        } finally {
            lock.unlock();
        }
    }

    public int getCapacity() {
        return capacity;
    }

    public void close() {
        lock.lock();
        try {
            logger.trace("RingBuffer.close()");
            open = false;
            consumer.signalAll();
            producer.signalAll();
        } finally {
            lock.unlock();
        }
    }
}
