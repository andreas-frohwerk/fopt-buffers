package de.frohwerk.fopt.io;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.Semaphore;

public class InMemoryPipe implements Pipe {
    private static final Logger logger = LogManager.getLogger(InMemoryPipe.class);

    private byte[] buf = new byte[7];
    private int off = 0;
    private int len = 0;

    private boolean closed = false;

    private final Semaphore free = new Semaphore(1);
    private final Semaphore available = new Semaphore(0);

    private final PipeInputStream pipeInputStream = new PipeInputStream();
    private final PipeOutputStream pipeOutputStream = new PipeOutputStream();

    @Override
    public PipeInputStream getInputStream() {
        return pipeInputStream;
    }

    @Override
    public PipeOutputStream getOutputStream() {
        return pipeOutputStream;
    }

    protected class PipeInputStream extends InputStream {
        @Override
        public int read() {
            while (!closed || len > 0) {
                try {
                    available.acquire();
                    final byte b = buf[off];
                    len = len - 1;
                    if (len == 0) {
                        off = 0;
                        free.release();
                    } else {
                        off = off + 1;
                        available.release();
                    }
                    return b;
                } catch (final InterruptedException ex) {
                    Thread.currentThread().interrupt();
                }
            }
            return -1;
        }

        @Override
        public int read(byte[] b) {
            if (b == null) throw new NullPointerException("Buffer may not be null");
            return read(b, 0, b.length);
        }

        @Override
        public int read(byte[] b, int offset, int length) {
            if (b == null) throw new NullPointerException("Buffer may not be null");
            if (length > b.length) throw new IllegalArgumentException("Buffer size is smaller than requested length");
            logger.info("PipeInputStream.read(byte[{}], {}, {})", b.length, offset, length);
            while (!closed || len > 0) {
                try {
                    available.acquire();
                    final int chunk = Math.min(len, length);
                    logger.debug("System.arraycopy(byte[{}], {}, byte[{}], {}, {})", buf.length, off, b.length, offset, chunk);
                    System.arraycopy(buf, off, b, offset, chunk);
                    len = len - chunk;
                    if (len == 0) {
                        off = 0;
                        free.release();
                    } else {
                        off = off + chunk;
                        available.release();
                    }
                    return chunk;
                } catch (final InterruptedException ex) {
                    Thread.currentThread().interrupt();
                }
            }
            return -1;
        }
    }

    protected class PipeOutputStream extends OutputStream {
        @Override
        public void write(int b) {
            while (true) {
                try {
                    free.acquire();
                    buf[0] = (byte) b;
                    len = len + 1;
                    available.release();
                    return;
                } catch (final InterruptedException ex) {
                    Thread.currentThread().interrupt();
                }
            }
        }

        @Override
        public void write(byte[] b) {
            if (b == null) throw new NullPointerException("Buffer may not be null");
            write(b, 0, b.length);
        }

        @Override
        public void write(byte[] b, int offset, int length) {
            if (b == null) throw new NullPointerException("Buffer may not be null");
            if (length > b.length) throw new NullPointerException("Length may not be larger than the buffer size");
            logger.info("PipeInputStream.write(byte[{}], {}, {})", b.length, offset, length);
            while (length > 0) {
                try {
                    free.acquire();
                    final int chunk = Math.min(buf.length, length);
                    logger.debug("System.arraycopy(byte[{}], {}, byte[{}], {}, {})", b.length, offset, buf.length, 0, chunk);
                    System.arraycopy(b, offset, buf, 0, chunk);
                    len += chunk;
                    length -= chunk;
                    offset += chunk;
                    available.release();
                } catch (final InterruptedException ex) {
                    Thread.currentThread().interrupt();
                }
            }
        }

        @Override
        public void close() {
            logger.info("PipeOutputStream.close()");
            while (true) {
                try {
                    free.acquire();
                    closed = true;
                    available.release();
                    return;
                } catch (final InterruptedException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
}
