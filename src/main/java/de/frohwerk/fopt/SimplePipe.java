package de.frohwerk.fopt;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.Semaphore;

public class SimplePipe implements Pipe {
    private static final Logger logger = LogManager.getLogger(SimplePipe.class);

    private boolean closed = false;

    private byte[] buffer = new byte[10];

    private int bufferUsed = 0;
    private int bufferOffset = 0;

    private final Semaphore free = new Semaphore(1);
    private final Semaphore available = new Semaphore(0);

    @Override
    public void send(final byte[] message) {
        if (closed) throw new IllegalStateException("Pipe is closed");
        for (int remaining = message.length; remaining > 0; ) {
            try {
                free.acquire();
                System.arraycopy(message, message.length - remaining, buffer, 0, buffer.length);
                bufferUsed = Math.min(remaining, buffer.length);
                remaining -= bufferUsed;
                logger.info("Sending {} bytes ({} remaining)", bufferUsed, remaining);
                available.release();
            } catch (final InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
        }
    }

    @Override
    public int receive(byte[] buf, int offset, final int length) {
        while (!closed || bufferUsed > 0) {
            try {
                available.acquire();
                final int len = Math.min(bufferUsed, length);
                System.arraycopy(buffer, this.bufferOffset, buf, offset, len);
                logger.info("Received {} bytes", len);
                bufferUsed -= len;
                if (bufferUsed == 0) {
                    bufferOffset = 0;
                    free.release();
                } else {
                    bufferOffset += len;
                    available.release();
                }
                return len;
            } catch (final InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
        }
        return -1;
    }

    @Override
    public synchronized void close() {
        closed = true;
        notifyAll();
    }
}
