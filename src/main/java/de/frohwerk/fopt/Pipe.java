package de.frohwerk.fopt;

public interface Pipe extends AutoCloseable {
    void send(final byte[] message);

    int receive(byte[] buf, int offset, final int length);

    @Override
    void close();
}
